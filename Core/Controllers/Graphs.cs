﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Core.Assets.Models;
using Core.Database;
using Core.Database.Enums;
using Core.Database.QueryLanguages;
using Core.Database.Tables;
using GraphQL;
using GraphQL.EntityFramework;
using GraphQL.Types;
using Microsoft.EntityFrameworkCore;

namespace Core.Controllers
{
   

    public class Schema : GraphQL.Types.Schema
    {
        public Schema(IDependencyResolver resolver) : base(resolver)
        {
            Query = resolver.Resolve<Query>();
            //Subscription = resolver.Resolve<Subscription>();
        }
    }

    public class Query : QueryGraphType<BeawreContext>
    {

        public Query(IEfGraphQLService<BeawreContext> efGraphQlService) : base(efGraphQlService)
        { 
            AddQueryField(
                name: "users",
                resolve: context => context.DbContext.User.Where(x => !x.IsDeleted)
                );

            Field<ListGraphType<AssetGraphQl>>(
                name: "groups",
                resolve: context =>
                {
                    var dbContext = (BeawreContext)context.UserContext;
                    var relationships = dbContext.Relationship.Where(x => x.Object1Type == ObjectType.AssetGroup && !x.IsDeleted).Select(x => x.Object1Id).ToArray();
                    return dbContext.Assets.Where(x => relationships.Contains(x.Id) && !x.IsDeleted && x.IsGroup).ToList();
                });

            AddQueryField(
                name: "assets",
                resolve: context => context.DbContext.Assets.Where(x => !x.IsDeleted)
            );

            AddQueryField(
                name: "edges",
                resolve: context => context.DbContext.Relationship.Where(x => x.Object1Type == ObjectType.Asset && x.Object2Type == ObjectType.Asset).Where(x => !x.IsDeleted)
            );


        }
    }
}
