﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Database;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;

namespace Core.Controllers.OData
{
    public class AssetsODataController : ODataController
    {
        private IBeawreContext _beawreContext;

        public AssetsODataController(IBeawreContext beawreContext)
        {
            _beawreContext = beawreContext;
        }

        [EnableQuery]
        public IActionResult Get()
        {
            return Ok(_beawreContext.Assets);
        }
    }
}
