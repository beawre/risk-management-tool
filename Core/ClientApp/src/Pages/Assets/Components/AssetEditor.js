import React from "react";
import { withTranslation } from "react-i18next";
import * as _ from "lodash";
import { Row, Col } from "antd";
import GGEditor, { Flow } from "gg-editor";
import FlowToolbar from "../../../components/Editor/FlowToolbar";
import FlowItemPanelArc from "../../../components/Editor/FlowItemsPanelArc";
import FlowDetailPanel from "../../../components/Editor/FlowDetailPanel";

import BackendService from "./../../../components/BackendService";

class AssetEditor extends React.Component {
  assetsApi = new BackendService("assets");

  constructor(props) {
    super(props);

    this.state = {
      toolbarItems: props.toolbarItems,
      itemsPanel: props.itemsPanel,
      graphData: props.graphData
    };
  }

  saveNewAssetObject = (type, model) => {
    switch (type) {
      case "node":
        this.assetsApi
          .post("create", {
            name: `${model.label} ${this.state.graphData.nodes.length}`,
            payloadData: { color: model.color, shape: model.shape, size: model.size, x: model.x, y: model.y }
          })
          .then(result => {
            this.props.loadData();
          });
        break;
      case "edge":
        this.assetsApi
          .post("createEdge", {
            Asset1Guid: model.source,
            Asset1Anchor: model.sourceAnchor,
            Asset2Guid: model.target,
            Asset2Anchor: model.targetAnchor
          })
          .then(result => {
            this.props.loadData();
          });
        break;
      case "addGroup":
        this.assetsApi.post("createGroup", { name: `Group ${this.state.graphData.groups.length}`, assets: model }).then(result => {
          this.props.loadData();
        });
        break;
      case "unGroup":
        this.assetsApi.post("removeGroup", { id: model }).then(result => {
          this.props.loadData();
        });
        break;
      default:
        break;
    }
  };

  render() {
    return (
      <GGEditor
        onAfterCommandExecute={({ command }) => {
          if (!_.isUndefined(command.addId)) {
            this.saveNewAssetObject(command.type, command.addModel);
          }
          if (!_.isUndefined(command.addGroupId)) {
            this.saveNewAssetObject(command.name, command.selectedItems);
          }
          if (command.command === "unGroup") {
            this.saveNewAssetObject(command.name, command.selectedItems[0]);
          }
        }}>
        <Row>
          <Col span={24}>
            <FlowToolbar items={this.state.toolbarItems} />
          </Col>
        </Row>
        <Row>
          {!_.isEmpty(this.state.itemsPanel) && (
            <Col span={4}>
              <FlowItemPanelArc items={this.state.itemsPanel} />
            </Col>
          )}
          <Col span={_.isEmpty(this.state.itemsPanel) ? 24 : 16}>
            <Flow
              onDrop={e => {
                if (e.currentItem !== undefined && e.currentItem !== null) {
                  this.assetsApi
                    .post("movePosition", {
                      assetId: e.currentItem.id,
                      x: e.x,
                      y: e.y
                    })
                    .then(result => {
                      this.props.loadData();
                      return true;
                    });
                }
              }}
              data={this.state.graphData}
              style={{ height: 600 }}
            />
          </Col>
          {!_.isEmpty(this.state.itemsPanel) && (
            <Col span={4}>
              <FlowDetailPanel />
            </Col>
          )}
        </Row>
      </GGEditor>
    );
  }
}

export default withTranslation()(AssetEditor);
