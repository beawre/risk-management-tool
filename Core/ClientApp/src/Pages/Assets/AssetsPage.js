import React from "react";
import { withTranslation } from "react-i18next";
import { observer } from "mobx-react";
import "storm-react-diagrams/dist/style.min.css";
import { Card, Tabs, Icon } from "antd";
import "./AssetsPage.css";
import * as _ from "lodash";
import AssetsStore from "./AssetsStore";
import { withPropsAPI } from "gg-editor";

import BackendService from "../../components/BackendService";

import AssetList from "./Components/AssetList";
import AssetEditor from "./Components/AssetEditor";
import AssetKanban from "./Components/AssetKanban";

class AssetsPage extends React.Component {
  assetsApi = new BackendService("assets");
  graphqlApi = new BackendService("graphql");

  store = AssetsStore;
  state = {
    nodes: [],
    groups: [],
    edges: [],
    graphData: null,
    isLoading: true
  };

  componentDidMount() {
    this.loadData();
  }

  loadData = () => {
    this.setState({ isLoading: true }, () => {
      this.graphqlApi
        .get(
          `get?query={assets{id,name,payload,group,vulnerabilities{id,name},risks{id},treatments{id,type,description}},edges{id,object1Id,object2Id,payload},groups{id,name,vulnerabilities{id,name},risks{id},treatments{id,type,description}}}`
        )
        .then(results => {
          var nodes = results.data.assets;
          _.forEach(results.data.groups, group => {
            nodes.push(group);
          });
          this.setState({
            nodes: nodes,
            graphData: this.getNodesAndEdgesData(results.data.assets, results.data.edges, results.data.groups),
            isLoading: false
          });
        });
    });
  };

  getNodesAndEdgesData = (nodes, edges, groups) => {
    let data = {
      nodes: [],
      edges: [],
      groups: []
    };

    _.forEach(groups, (group, index) => {
      data.groups.push({
        id: group.id,
        label: group.name,
        index: 0,
        isCollapsed: false
      });
    });

    _.forEach(nodes, (node, index) => {
      if (node.payload === undefined || node.payload === "null") {
        return true;
      }
      console.log(node);
      var payload = JSON.parse(node.payload);
      data.nodes.push({
        type: "node",
        size: payload.Size,
        shape: payload.Shape,
        color: payload.Color,
        label: node.name + " / " + index,
        x: payload.X,
        y: payload.Y,
        id: node.id,
        index: index + 1,
        parent: node.group
      });
    });

    _.forEach(edges, (edge, index) => {
      var payload = JSON.parse(edge.payload);
      data.edges.push({
        source: edge.object1Id,
        sourceAnchor: payload.Asset1Anchor,
        target: edge.object2Id,
        targetAnchor: payload.Asset2Anchor,
        id: edge.id,
        index: index + 1
      });
    });
    return data;
  };

  extractView(layout) {
    // console.log("layout", layout);
    if (layout === undefined || layout === null) return;
    var layouts = [];
    _.forEach(Object.keys(layout), item => {
      layouts.push(this.extractElement(item, layout[item]));
    });
    return layouts;
  }

  extractElement(type, layout) {
    switch (type.toLowerCase()) {
      case "tabs":
        return (
          <Tabs type="line">
            {layout.map(tabData => (
              <Tabs.TabPane
                tab={
                  <span>
                    <Icon type={tabData.icon} /> {this.props.t(tabData.title)}
                  </span>
                }
                key={tabData.key}>
                {this.extractView(layout[layout.indexOf(tabData)].components)}
              </Tabs.TabPane>
            ))}
          </Tabs>
        );
      case "assetlist":
        return <AssetList nodes={this.state.nodes} />;
      case "ggeditor":
        return <AssetEditor itemsPanel={layout.itemPanels} toolbarItems={layout.toolbarItems} graphData={this.state.graphData} loadData={this.loadData} />;
      case "assetkanban":
        console.log("kanbanData", this.state.kanbanData);
        return <AssetKanban data={this.state.kanbanData} columns={layout.columns} columnsOrder={layout.columnsOrder} assets={this.state.nodes} />;
      default:
        return {};
    }
  }

  extractPage(page) {
    const appConfig = JSON.parse(localStorage.getItem("appConfig"));
    return appConfig.pages[page];
  }

  render() {
    const pageSettings = this.extractPage("assets");
    return (
      <React.Fragment>
        {this.state.isLoading === false && (
          <div>
            <Card style={{ height: "100%" }}>
              <h2>{this.props.t(pageSettings.title)}</h2>
            </Card>

            <Card style={{ height: "100%" }}>{this.extractView(pageSettings.layout)}</Card>
          </div>
        )}
      </React.Fragment>
    );
  }
}

export default withTranslation()(observer(withPropsAPI(AssetsPage)));
