import React, { Component } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import "antd/dist/antd.css";
import DocumentTitle from "react-document-title";
import { withTranslation } from "react-i18next";

import BaseLayout from "./components/Layout/BaseLayout";
import SettingsPage from "./Pages/SettingsPage";
import AssetsPage from "./Pages/Assets/AssetsPage";
import { Spin } from "antd";
import AssetsAnalysisPage from "./Pages/AssetsAnalysis/AssetsAnalysisPage";

import ConfigStore from "./ConfigStore";
import { UserAgentApplication } from "msal";
import BackendService from "./components/BackendService";

const AppRoute = ({ component: Component, layout: Layout, title: Title, ...rest }) => (
  <Route
    {...rest}
    render={props => (
      <DocumentTitle title={Title}>
        <Layout pageTitle={Title}>
          <Component {...props} />
        </Layout>
      </DocumentTitle>
    )}
  />
);

export const msalInstance = new UserAgentApplication({
  auth: {
    clientId: "1535f037-ae41-4498-ab94-9138d86d244a",
    authority: "https://login.microsoftonline.com/organizations"
  },
  cache: {
    cacheLocation: "localStorage",
    storeAuthStateInCookie: true
  }
});

class App extends Component {
  static displayName = App.name;
  userApi = new BackendService("users");

  constructor() {
    super();
    this.state = {
      isLoadingUser: true,
      isLoadingConfig: true
    };
  }

  startSession(history) {
    this.authService.handleAuthentication(history);
    return <Spin />;
  }

  componentDidMount() {
    ConfigStore.getAppConfig().then(r => {
      localStorage.setItem("appConfig", JSON.stringify(r));
      this.setState({ isLoadingConfig: false });
    });

    msalInstance.handleRedirectCallback(
      () => {
        // on success
        this.setState({
          authenticated: true
        });
      },
      (authErr, accountState) => {
        // on fail
        console.log(authErr);

        this.setState({
          hasError: true,
          errorMessage: authErr.errorMessage
        });
      }
    );

    // if we are inside renewal callback (hash contains access token), do nothing
    if (msalInstance.isCallback(window.location.hash)) {
      this.setState({
        renewIframe: true
      });
      return;
    }

    // not logged in, perform full page redirect
    if (!msalInstance.getAccount()) {
      msalInstance.loginRedirect({});
      return;
    } else {
      var account = msalInstance.getAccount();
      this.userApi.get(`Get/${account.accountIdentifier}`).then(user => {
        if (user.status === 404) {
          this.userApi
            .post("create", { accountId: account.accountIdentifier, email: account.userName, username: account.name })
            .then(r => r.json())
            .then(createdUser => {
              this.loginUser(createdUser.id);
            });
        } else {
          this.loginUser(user.id);
        }
      });
    }
  }

  loginUser(userId) {
    localStorage.setItem("userId", userId);
    this.setState({ isLoadingUser: false });
  }

  state = {
    isLoggedIn: false,
    message: ""
  };

  render() {
    const appConfig = JSON.parse(localStorage.getItem("appConfig"));
    const componentsRegistry = {
      AssetsPage: AssetsPage,
      AssetsAnalysisPage: AssetsAnalysisPage,
      SettingsPage: SettingsPage
    };

    return (
      <div>
        {!this.state.isLoadingUser && !this.state.isLoadingConfig && (
          <Switch>
            {appConfig.router.paths.map(e => (
              <AppRoute key={e.path} exact layout={BaseLayout} title={this.props.t(e.title)} path={e.path} component={componentsRegistry[e.component]} />
            ))}
            <Route exect path="/" render={() => <Redirect to={appConfig.router.defaultRoute} />} />
          </Switch>
        )}
      </div>
    );
  }
}

export default withTranslation()(App);
