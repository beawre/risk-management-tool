import React from "react";
import { Divider } from "antd";
import { Toolbar } from "gg-editor";
import ToolbarButton from "./ToolbarButton";
import "./FlowToolbar.css";

const FlowToolbar = items => {
  return (
    <Toolbar className="toolbar">
      {items.items.map(item => (item.type === "button" ? <ToolbarButton command={item.command} icon={item.icon} text={item.text} /> : <Divider type="vertical" />))}
    </Toolbar>
  );
};

export default FlowToolbar;
