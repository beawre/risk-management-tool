import ConfigStore from "../ConfigStore";
// import AuthService from "./Auth/AuthService";

export default class BackendService {
  basePath = "";
  accessToken = "";
  //   auth = new AuthService();
  config = ConfigStore;

  constructor(path) {
    this.basePath = this.config.backendUrl + path;

    this.accessToken = localStorage.getItem("userId"); //this.auth.getAccessToken();
  }

  getUrl(url) {
    return url === "" ? "" : url.toString().substring(0, 1) === "?" ? url : "/" + url;
  }

  get(url = "") {
    return fetch(this.basePath + this.getUrl(url), {
      headers: new Headers({
        // "Content-Type": "application/json",
        Accept: "application/json",
        Authorization: `Bearer ${this.accessToken}`
      })
    }).then(response => response.json());
  }

  post(url = "", payload) {
    return fetch(this.basePath + this.getUrl(url), {
      headers: new Headers({
        "Content-Type": "application/json",
        Accept: "application/json",
        Authorization: `Bearer ${this.accessToken}`
      }),
      method: "POST",
      body: JSON.stringify(payload)
    });
  }

  put(url = "", payload) {
    return fetch(this.basePath + this.getUrl(url), {
      headers: new Headers({
        "Content-Type": "application/json",
        Accept: "application/json",
        Authorization: `Bearer ${this.accessToken}`
      }),
      method: "PUT",
      body: JSON.stringify(payload)
    });
  }

  delete(url = "") {
    return fetch(this.basePath + this.getUrl(url), {
      headers: new Headers({
        "Content-Type": "application/json",
        Accept: "application/json",
        Authorization: `Bearer ${this.accessToken}`
      }),
      method: "DELETE"
    });
  }
}
