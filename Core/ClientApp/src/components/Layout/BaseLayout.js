import React from "react";
import { Layout, Col, Row } from "antd";
import "./BaseLayout.css";

//import MainMenu from './MainMenu';
//import BaseLayoutStore from './BaseLayoutStore';
import { observer } from "mobx-react";
import { withTranslation } from "react-i18next";
import MainMenu from "./MainMenu";

class BaseLayout extends React.Component {
  state = {
    collapsed: false
  };

  onCollapse = collapsed => {
    this.setState({ collapsed });
  };

  cleanSession = () => {
    localStorage.clear();
    window.location.reload();
  };

  render() {
    const appConfig = JSON.parse(localStorage.getItem("appConfig"));
    return (
      <Layout>
        <Layout>
          <Layout.Header style={{ background: "#fff", padding: 0, boxShadow: "0 1px 4px rgba(0,21,41,.08)" }}>
            <Row>
              <Col span={4}>
                <img src={appConfig.header.path} alt="Enact" style={{ marginTop: "-5px", height: "65px", marginLeft: "5px" }} />
              </Col>
              <Col span={2}>{this.props.pageTitle || "Page"}</Col>
              <Col span={16}>
                <MainMenu />
              </Col>
            </Row>
          </Layout.Header>
          <Layout.Content style={{ margin: "24px" }}>{this.props.children}</Layout.Content>
          <Layout.Footer style={{ textAlign: "center" }}>
            <small>{appConfig.footer.text}</small>
          </Layout.Footer>
        </Layout>
      </Layout>
    );
  }
}

export default withTranslation()(observer(BaseLayout));
