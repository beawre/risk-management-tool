﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Database.Enums
{
    public enum ObjectType
    {
        Asset = 100,
        AssetGroup = 600,
        Vulnerabilitie = 200, 
        Risk = 300, 
        Treatment = 400,
        Container = 500,
        AssetEdge = 700,
    }
}
