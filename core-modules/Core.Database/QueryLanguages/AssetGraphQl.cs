﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Database;
using Core.Database.Enums;
using Core.Database.Tables;
using GraphQL.EntityFramework;
using GraphQL.Types;

namespace Core.Database.QueryLanguages
{
    public class AssetGraphQl : EfObjectGraphType<BeawreContext, Asset>
    {
        public AssetGraphQl(IEfGraphQLService<BeawreContext> graphQlService) : base(graphQlService)
        {
            Field(x => x.Id);

            Field(x => x.Name, true);
            Field(x => x.Description, true);
            Field(x => x.Payload, true);

            Field<ListGraphType<VulnerabilityGraphQl>>(
                name: "vulnerabilities",
                resolve: context =>
                {
                    var dbContext = (BeawreContext)context.UserContext;
                    var relationships = dbContext.Relationship.Where(x => x.Object2Type == ObjectType.Vulnerabilitie && x.Object1Type == ObjectType.Asset && x.Object1Id == context.Source.Id && !x.IsDeleted).Select(x => x.Object2Id).ToArray();
                    return dbContext.Vulnerability.Where(x => relationships.Contains(x.Id) && !x.IsDeleted).ToList();
                });
            Field<ListGraphType<RisksGraphQl>>(
                name: "risks",
                resolve: context =>
                {
                    var dbContext = (BeawreContext)context.UserContext;
                    var relationships = dbContext.Relationship.Where(x => x.Object2Type == ObjectType.Risk && x.Object1Type == ObjectType.Asset && x.Object1Id == context.Source.Id && !x.IsDeleted).Select(x => x.Object2Id).ToArray();
                    return dbContext.Risk.Where(x => relationships.Contains(x.Id) && !x.IsDeleted).ToList();
                });
            Field<ListGraphType<TreatmentsGraphQl>>(
                name: "treatments",
                resolve: context =>
                {
                    var dbContext = (BeawreContext)context.UserContext;
                    var relationships = dbContext.Relationship.Where(x => x.Object2Type == ObjectType.Treatment && x.Object1Type == ObjectType.Asset && x.Object1Id == context.Source.Id && !x.IsDeleted).Select(x => x.Object2Id).ToArray();
                    return dbContext.Treatment.Where(x => relationships.Contains(x.Id) && !x.IsDeleted).ToList();
                });

            Field<StringGraphType >(
                name: "group",
                resolve: context =>
                {
                    var dbContext = (BeawreContext)context.UserContext;
                    return dbContext.Relationship.FirstOrDefault(x => x.Object2Type == ObjectType.Asset && x.Object1Type == ObjectType.AssetGroup && x.Object2Id == context.Source.Id && !x.IsDeleted)?.Object1Id;
                });

            Field(x => x.IsDeleted);
            Field(x => x.CreatedDateTime);

        }
    }
}
