﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.Database;
using Core.Database.Tables;
using GraphQL.EntityFramework;

namespace Core.Database.QueryLanguages
{

    public class TreatmentsGraphQl : EfObjectGraphType<BeawreContext, Treatment>
    {
        public TreatmentsGraphQl(IEfGraphQLService<BeawreContext> graphQlService) : base(graphQlService)
        {
            Field(x => x.Id);
            Field(x => x.Type);
            Field(x => x.Description);
            Field(x => x.IsDeleted);
            Field(x => x.CreatedDateTime);

        }
    }
}
