﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Core.Database.Enums;

namespace Core.Database.Tables
{
    [Table("Relationship")]
    public class Relationship : EntityBase
    {
        public ObjectType Object1Type { get; set; }
        public Guid Object1Id { get; set; }

        public ObjectType Object2Type { get; set; }
        public Guid Object2Id { get; set; }

        public string Payload { get; set; }


    }
}
