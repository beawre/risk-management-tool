﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Core.Assets.Implementation.Commands.Risks;
using Core.Database;
using Core.Database.Tables;
using MediatR;
using Newtonsoft.Json;

namespace Core.Assets.Implementation.CommandHandlers.Risks
{
    public class CreateRiskCommandHandler : IRequestHandler<CreateRiskCommand, Risk>
    {
        private IBeawreContext _beawreContext;
        private IMapper _mapper;

        public CreateRiskCommandHandler(IBeawreContext beawreContext, IMapper mapper)
        {
            _beawreContext = beawreContext;
            _mapper = mapper;
        }

        public Task<Risk> Handle(CreateRiskCommand request, CancellationToken cancellationToken)
        {
            var entity = _mapper.Map<Risk>(request);

            entity.Payload = JsonConvert.SerializeObject(request.PayloadData);

            _beawreContext.Risk.Add(entity);
            _beawreContext.SaveChanges();

            return Task.FromResult(entity);
        }
    }
}
