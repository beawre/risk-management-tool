﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Core.Assets.Implementation.Commands.Treatments;
using Core.Database;
using Core.Database.Tables;
using MediatR;

namespace Core.Assets.Implementation.CommandHandlers.Treatments
{
    public class CreateTreatmentCommandHandler : IRequestHandler<CreateTreatmentCommand, Treatment>
    {
        private IMapper _mapper;
        private IBeawreContext _beawreContext;

        public CreateTreatmentCommandHandler(IMapper mapper, IBeawreContext beawreContext)
        {
            _mapper = mapper;
            _beawreContext = beawreContext;
        }

        public Task<Treatment> Handle(CreateTreatmentCommand request, CancellationToken cancellationToken)
        {
            var entity = _mapper.Map<Treatment>(request);

            _beawreContext.Treatment.Add(entity);
            _beawreContext.SaveChanges();

            return Task.FromResult(entity);
        }
    }
}
