﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Core.Assets.Implementation.Commands;
using Core.Assets.Implementation.Commands.Assets;
using Core.Assets.Interfaces.Services;
using Core.Database.Tables;
using MediatR;

namespace Core.Assets.Implementation.Services
{
    public class AssetService : IAssetService
    {
        private IMediator _mediator;

        public AssetService(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<Asset> Create(CreateAssetCommand command) => await _mediator.Send(command);

        public async Task<bool> MovePosition(UpdateAssetPositionCommand command) => await _mediator.Send(command);
    }
}
