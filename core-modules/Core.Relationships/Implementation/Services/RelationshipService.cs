﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Core.Database.Tables;
using Core.Relationships.Implementation.Commands;
using Core.Relationships.Interfaces.Services;
using MediatR;

namespace Core.Relationships.Implementation.Services
{
    public class RelationshipService : IRelationshipService
    {
        private IMediator _mediator;

        public RelationshipService(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<Relationship> Create(CreateRelationshipCommand command) => await _mediator.Send(command);
    }
}
